% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% Main programm can simulate automatic control of an UAV
tic

%% Clean workspace
clear all;
close all;
clc;

%% Add Paths for trajectory generation
addpath TrajectoryGeneration
addpath ControlAllPID
addpath ControlMPC
addpath ControlNMPC

%% Initialize Simulation
InitPlot;
InitPlotQuadModel;

%% Intialize variable
InitVariables;
ModeleDynamiqueNonLineaire;
SaveForPlot; % save init - counter = 1 --> t = 0s
Oscar.counter = Oscar.counter + 1; 

while Oscar.t_plot(Oscar.counter-1) < max(Oscar.t_plot)
    
    %% Trajectory generation   
    ComputeDesiredPositionValueForTracking;
    
    %% Measure Parameters -- for simulating sensors errors
    if(Oscar.WantNoiseMeasurment)
    SensorsAddNoise
    end
    
    %% Observateur -- fusion to add more realistic data of sensors
    %------!------------------------------!----------%
    % Filter Measurements -- implement observateur EKF here
    %------!------------------------------!----------%
    
    %% Implement Controllers
    ControllersRun;  
    
    %% Calculate Desired Motor Speeds
    QuadMotorSpeed;
    
    %% Update Position With The Equations of Motion
    ModeleDynamiqueNonLineaire; 
    SaveForPlot;
    Oscar.counter = Oscar.counter + 1;
    
    %% Plot the Quadrotor's Position (tout les 3)
    if(mod(Oscar.counter,3)==0)
        MAJPlotQuadrirotor; 
        hold on
         head = scatter3(Oscar.X_des,-Oscar.Y_des,-Oscar.Z_des);
        drawnow
         delete(head)
    end
  
    Oscar.init = 1;  % End initialization after first simulation iteration
    
end

%% Plot Data
PlotData;
toc