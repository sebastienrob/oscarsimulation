% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% Initialisation simulation environnment

function InitPlot

axes('units','normalized','position',[.2 .1 .6 .8]);
axis equal

axis([-3 3 -3 3 -3 3]);
view(40,40)
grid on
hold on

camproj perspective 
camva(5)
hlight = camlight('headlight'); 
lighting gouraud
set(gcf,'Renderer','OpenGL')
line([-1 1],[0 0],[0 0])
line([0 0],[-.5 .5],[0 0],'color','r')

xlabel('x')
ylabel('y')
zlabel('z')
title('simulation of quadrorotor')

end