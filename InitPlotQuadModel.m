% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% This function sets the inital vertices and faces of the quadrotor arms
% and motors for drawing purposes. In this coordinate system, the front of
% the quadrotor is to the left and Motor 1 is the left motor.

% Patch create "graphic objects"

%% Store data in global variable
load Oscar

Oscar.X_arm = patch('xdata',Oscar.X_armX,'ydata',Oscar.X_armY,'zdata',Oscar.X_armZ,'facealpha',.4,'facecolor','k');
Oscar.Y_arm = patch('xdata',Oscar.Y_armX,'ydata',Oscar.Y_armY,'zdata',Oscar.Y_armZ,'facealpha',.4,'facecolor','k');
Oscar.Motor1 = patch('xdata',Oscar.Motor1X,'ydata',Oscar.Motor1Y,'zdata',Oscar.Motor1Z,'facealpha',.3,'facecolor','g');
Oscar.Motor2 = patch('xdata',Oscar.Motor2X,'ydata',Oscar.Motor2Y,'zdata',Oscar.Motor2Z,'facealpha',.3,'facecolor','b');
Oscar.Motor3 = patch('xdata',Oscar.Motor3X,'ydata',Oscar.Motor3Y,'zdata',Oscar.Motor3Z,'facealpha',.3,'facecolor','b');
Oscar.Motor4 = patch('xdata',Oscar.Motor4X,'ydata',Oscar.Motor4Y,'zdata',Oscar.Motor4Z,'facealpha',.3,'facecolor','b');