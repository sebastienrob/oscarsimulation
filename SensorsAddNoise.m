% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% This function simulates sensor measurement noise for the GPS, barometer,
% and IMU. 
% The noise variances come from each sensor's respective datasheet. 

function SensorsAddNoise

global Oscar;

%% GPS Measurements
if(mod(Oscar.counter,Oscar.GPS_freq) == 0)
    Oscar.X = Oscar.X + randn(1)*Oscar.X_error;
    Oscar.Y = Oscar.Y + randn(1)*Oscar.Y_error;
    
    %% Barometer Measurements
    
    Oscar.Z = Oscar.Z + randn(1)*Oscar.Z_error;
    
end

%% IMU Measurements

Oscar.X_ddot = Oscar.X_ddot + Oscar.x_acc_bias + Oscar.x_acc_sd*randn(1);
Oscar.Y_ddot = Oscar.Y_ddot + Oscar.y_acc_bias + Oscar.y_acc_sd*randn(1);
Oscar.Z_ddot = Oscar.Z_ddot + Oscar.z_acc_bias + Oscar.z_acc_sd*randn(1);

Oscar.p = Oscar.p + Oscar.x_gyro_bias + Oscar.x_gyro_sd*randn(1);
Oscar.q = Oscar.q + Oscar.y_gyro_bias + Oscar.y_gyro_sd*randn(1);
Oscar.r = Oscar.r + Oscar.z_gyro_bias + Oscar.z_gyro_sd*randn(1);

end