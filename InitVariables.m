% S�bastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% Initialization of quadrorotor system and simulation parameters.
% quadrotor�s physical parameters, initial and desired conditions,
% simulation parameters, and controller gains.

% WARNING : when you change init state, change init and desired values too

global Oscar;

%% Simulation Parameters
Oscar.init = 0;     % used in initilization 
Oscar.Ts = .01;     % Sampling time (100 Hz)
Oscar.sim_time = 20; % Simulation time (seconds)
Oscar.DurationWayPoint = 15; % if we want stabilize around ControlPoint / 0 if continue trajectory
Oscar.counter = 1; % the counter that holds the time value

%% Generation trajectory choice
Oscar.PolynomeTrajectory = 0; % change xf, yf, zf for this choice
Oscar.LemniscapeTrajectory = 0; %change y_des et y init fot this choice
Oscar.CircleTrajectory = 1; % change x_des et x init for this choice
Oscar.TournadeTrajectory = 0;

%% Controllers choice
Oscar.PositionPID = 0; % POSITION control
Oscar.PositionLMPC = 1;
Oscar.AltitudePID = 0; % ALTITUDE control
Oscar.AltitudeLMPC = 1;
Oscar.AttitudePID = 1; % ATTITUDE control
Oscar.AttitudeLMPC = 0;
Oscar.MotorsRatePID = 1; % MOTORS/RATE control

%% Plotting Variables
Oscar.t_plot = 0:Oscar.Ts:Oscar.sim_time; % the time values
Oscar.Xtemp = 0;     % Temp variables used rotating and plotting quadrotor
Oscar.Ytemp = 0;     % Temp variables used rotating and plotting quadrotor
Oscar.Ztemp = 0;     % Temp variables used rotating and plotting quadrotor

%% Environmental Parametes
Oscar.g = 9.81;     % Gravity (m/s^2)

%% Quadrotor Physical Parameters GLOBAL
Oscar.m = 1.4;      % Quadrotor mass (kg)
Oscar.l = .56;     % Distance from the center of mass to the each motor (m)
Oscar.t = .02;   % Thickness of the quadrotor's arms for drawing purposes (m)
Oscar.rot_rad = .1;   %Radius of the propellor (m)
Oscar.Kd = 1.3858e-6;    % Drag torque coeffecient (kg-m^2)

Oscar.Kdx = 0.16481;    % Translational drag force coeffecient (kg/s)
Oscar.Kdy = 0.31892;    % Translational drag force coeffecient (kg/s)
Oscar.Kdz = 1.1E-6;    % Translational drag force coeffecient (kg/s)

Oscar.Jx = .05;     % Moment of inertia about X axis (kg-m^2)
Oscar.Jy = .05;     % Moment of inertia about Y axis (kg-m^2)
Oscar.Jz = .24;    % Moment of inertia about Z axis (kg-m^2)

%% Quadrotor Sensor Paramaters SENSOR

Oscar.WantNoiseMeasurment = 0; % if 0, we have perfect sensor measurements

Oscar.GPS_freq = (1/Oscar.Ts)/1;  
Oscar.X_error = .01;  %+/- m
Oscar.Y_error = .01;  %+/- m
Oscar.Z_error = .02;  %+/- m

Oscar.x_acc_bias = 0.16594;  % m/s^2
Oscar.x_acc_sd = 0.0093907;
Oscar.y_acc_bias = 0.31691;  % m/s^2
Oscar.y_acc_sd = 0.011045;
Oscar.z_acc_bias = -8.6759;  % m/s^2
Oscar.z_acc_sd = 0.016189;

Oscar.x_gyro_bias = 0.00053417;  % rad/s
Oscar.x_gyro_sd = 0.00066675; %standard d�viation rad/s
Oscar.y_gyro_bias = -0.0011035;  % rad/s
Oscar.y_gyro_sd = 0.00053642;
Oscar.z_gyro_bias = 0.00020838;  % rad/s
Oscar.z_gyro_sd = 0.0004403;

%% Motor Parameters MOTOR
Oscar.KT = 1.3328e-5;    % Thrust force coeffecient (kg-m)
Oscar.Jr = 0.044;     % Moment of Inertia of the rotor (kg-m^2)
Oscar.max_motor_speed = 925; % motors upper limit (rad/s)
Oscar.min_motor_speed = 0; %-1*((400)^2); % motors lower limit (can't spin in reverse)

Oscar.Obar = 0;     % sum of motor speeds (O1-O2+O3-O4, N-m) 
Oscar.O1 = 0;       % Front motor speed (radians/s)
Oscar.O2 = 0;       % Right motor speed (radians/s)
Oscar.O3 = 0;       % Rear motor speed (radians/s)
Oscar.O4 = 0;       % Left motor speed (radians/s)

%% Translational Positions Init
Oscar.X = 1.99;        % Initial position in X direction WF (m) (1.99 for circle)
Oscar.Y = 0;        % Initial position in Y direction WF (m) (1 for lemniscape)
Oscar.Z = 0;        % Initial position in Z direction WF (m)
Oscar.Xf = 2;        % final position in X direction WF (m)
Oscar.Yf = 2;        % final position in Y direction WF (m)
Oscar.Zf = -3;        % final position in Z direction WF (m)
Oscar.X_BF = 0;     % Initial position in X direction BF (m)
Oscar.Y_BF = 0;     % Initial position in Y direction BF (m)
Oscar.Z_BF = 0;     % Initial position in the Z direction BF (m)

%% Translational Velocities
Oscar.X_dot = 0;    % Initial velocity in X direction WF (m/s)
Oscar.Y_dot = 0;    % Initial velocity in Y direction WF (m/s)
Oscar.Z_dot = 0;    % Initial velocity in Z direction WF (m/s)
Oscar.X_dot_BF = 0;    % Initial velocity in X direction BF (m/s)
Oscar.Y_dot_BF = 0;    % Initial velocity in Y direction BF (m/s)
Oscar.Z_dot_BF = 0;    % Initial velocity in Y direction BF (m/s)

%% Angular Positions in WF
Oscar.phi = 0;      % Initial phi value (rotation about X GF, roll,  radians)
Oscar.theta = 0;    % Initial theta value (rotation about Y GF, pitch, radians)
Oscar.psi = 0;      % Initial psi value (rotation about Z GF, yaw, radians)

%% Angular Velocities in BF
Oscar.p = 0;        % Initial p value (angular rate rotation about X BF, radians/s)
Oscar.q = 0;        % Initial q value (angular rate rotation about Y BF, radians/s)
Oscar.r = 0;        % Initial r value (angular rate rotation about Z BF, radians/s)

%% Desired variables
Oscar.X_des = 1.99;         % desired value of X in World frame (1.99 for circle)
Oscar.Y_des = 0;         % desired value of Y in World frame  (1 for lemniscape)
Oscar.Z_des = 0;         % desired value of Z in World frame

Oscar.X_des_BF = 0;            % desired value of X in Body frame
Oscar.Y_des_BF = 0;            % desired value of Y in Body frame
Oscar.Z_des_BF = 0;            % desired value of Z in Body frame

Oscar.X_des_dot = 0;            % desired value of vX in Body frame
Oscar.Y_des_dot = 0;            % desired value of vY in Body frame
Oscar.Z_des_dot = 0;            % desired value of vZ in Body frame

Oscar.phi_des = 0;          % desired value of phi (radians)
Oscar.theta_des = 0;        % desired value of theta (radians)
Oscar.psi_des = 0;          % desired value of psi (radians)

%% Measured variables
Oscar.X_meas = 0;
Oscar.Y_meas = 0;
Oscar.Z_meas = 0;
Oscar.phi_meas = 0;
Oscar.theta_meas = 0;
Oscar.psi_meas = 0;

%% Disturbance Variables (drag, lift, ground effect)
Oscar.X_dis = 0;            % Disturbance in X direction
Oscar.Y_dis = 0;            % Disturbance in Y direction
Oscar.Z_dis = 0;            % Ddisturbance in Z direction
Oscar.phi_dis = 0;            % Disturbance in Yaw direction
Oscar.theta_dis = 0;            % Disturbance in Pitch direction
Oscar.psi_dis = 0;            % Disturbance in Roll direction

%% Control Inputs
Oscar.U1 = 0;       % Total thrust (N)
Oscar.U2 = 0;       % Torque about X axis BF (N-m)
Oscar.U3 = 0;       % Torque about Y axis BF (N-m)
Oscar.U4 = 0;       % Torque about Z axis BF (N-m)

%% Control Limits (update values)
Oscar.U1_max = 43.5;   % Quad.KT*4*Quad.max_motor_speed^2
Oscar.U1_min = 0;      
Oscar.G_max = Oscar.g - Oscar.U1_max/Oscar.m; % for ControlAltitudeMPC
Oscar.G_min = Oscar.g - Oscar.U1_min/Oscar.m; % for ControlAltitudeMPC
Oscar.U2_max = 6.25;  % Quad.KT*Quad.l*Quad.max_motor_speed^2
Oscar.U2_min = -6.25; % Quad.KT*Quad.l*Quad.max_motor_speed^2
Oscar.U3_max = 6.25;  % Quad.KT*Quad.l*Quad.max_motor_speed^2
Oscar.U3_min = -6.25; % Quad.KT*Quad.l*Quad.max_motor_speed^2
Oscar.U4_max = 2.25; % Quad.Kd*2*Quad.max_motor_speed^2
Oscar.U4_min = -2.25;% Quad.Kd*2*Quad.max_motor_speed^2

%% PID parameters Pose X, Y, Z
Oscar.X_KP = .35;          % KP value in X position control
Oscar.X_KI = 0.25;            % KI value in X position control
Oscar.X_KD = -.35;         % KD value in X position control
Oscar.X_KI_lim = .25;         % Error to start calculating integral term

Oscar.Y_KP = .35;          % KP value in Y position control
Oscar.Y_KI = 0.25;            % KI value in Y position control
Oscar.Y_KD = -.35;         % KD value in Y position control
Oscar.Y_KI_lim = .25;         % Error to start calculating integral term

Oscar.Z_KP = 10/1.7;    % KP value in altitude control
Oscar.Z_KI = 0*3;    % KI value in altitude control
Oscar.Z_KD = -10/1.980;  % KD value in altitude control
Oscar.Z_KI_lim = .25;         % Error to start calculating integral term

%% PID parameters Angular

Oscar.phi_KP = 4.5;      % KP value in roll control 2
Oscar.phi_KI = 0;       % KI value in roll control   1        
Oscar.phi_KD = 0;     % KD value in roll control  -.5
Oscar.phi_max = pi/6;   % Maximum roll angle commanded
Oscar.phi_KI_lim = 2*(2*pi/360);  % Error to start calculating integral 

Oscar.theta_KP = 4.5;    % KP value in pitch control 2
Oscar.theta_KI = 0;     % KI value in pitch control 1
Oscar.theta_KD = 0;   % KD value in pitch control -.5
Oscar.theta_max = pi/6; % Maximum pitch angle commanded
Oscar.theta_KI_lim = 2*(2*pi/360);  % Error to start calculating integral 

Oscar.psi_KP = 10;     % KP value in yaw control
Oscar.psi_KI = 0;     % KI value in yaw control .75
Oscar.psi_KD = 0;     % KD value in yaw control -.5
Oscar.psi_KI_lim = 8*(2*pi/360);  % Error to start calculating integral 

%% PID parameters Angular Velocity (in body frame)

Oscar.p_KP = 2.7;    % KP value in roll angular control 2
Oscar.p_KI = 1;     % KI value in roll angular control
Oscar.p_KD = -.01;   % KD value in roll angular control -.5
Oscar.p_max = 50*(2*pi/360); % Maximum roll angular angle commanded
Oscar.p_KI_lim = 10*(2*pi/360);  % Error to start calculating integral 

Oscar.q_KP = 2.7;    % KP value in pitch angular control
Oscar.q_KI = 1;     % KI value in pitch angular control
Oscar.q_KD = -.01;   % KD value in pitch angular control -.5
Oscar.q_max = 50*(2*pi/360); % Maximum pitch angular angle commanded
Oscar.q_KI_lim = 10*(2*pi/360);  % Error to start calculating integral 

Oscar.r_KP = 2.7;    % KP value in yaw angular control
Oscar.r_KI = 1;     % KI value in yaw angular control
Oscar.r_KD = -.01;   % KD value in yaw angular control
Oscar.r_max = 50*(2*pi/360); % Maximum yaw angular commanded
Oscar.r_KI_lim = 10*(2*pi/360);  % Error to start calculating integral

%% Generation trajectory parameters (compute init velocity)
Oscar.X_des_precedent = Oscar.X_des; % init velocity desired for trajectory init
Oscar.Y_des_precedent = Oscar.Y_des; % init velocity desired for trajectory init
Oscar.Z_des_precedent= Oscar.Z_des; % init velocity desired for trajectory init
Oscar.theta_des_precedent = Oscar.theta_des;
Oscar.phi_des_precedent = Oscar.phi_des;
Oscar.psi_des_precedent = Oscar.psi_des;
