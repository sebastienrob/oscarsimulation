% S�bastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% ----!---- CONTROL STEP 1 ----!----
% ----!---- LOW RATE FREQUENCE ----!----
% ----!---- Camera-SLAM(BF)/IMU-odometry(BF)/GPS(WF) or others sensors ----!----

% This function implements a PID Controller.
% use Zieger or other method for identification parameters

% A HIGH LEVEL controller outputs desired roll and pitch angles based on 
% errors between the World and desired X and Y positions.

function ControlPositionPID

persistent x_error_sum;
persistent y_error_sum;

global Oscar

%% initialize persistent variables at beginning of simulation
if Oscar.init==0
    x_error_sum = 0; % somme des erreurs futur pour l'int�gral
    y_error_sum = 0; % Int�gral appliqu� si une grande diff�rence de pose
end

%% High Level Position Controller DONNES CAPTEUR (ICI M.D.)

x = Oscar.X;
y = Oscar.Y;
z = Oscar.Z;
phi = Oscar.phi;
theta = Oscar.theta;
psi = Oscar.psi;

%% Rotate Desired Position from WF to BF (Z axis rotation only?)
[Oscar.X_des_BF,Oscar.Y_des_BF,Oscar.Z_des_BF] = RotateWFtoBF(Oscar.X_des,Oscar.Y_des,Oscar.Z_des,0*Oscar.phi_des,0*Oscar.theta_des,Oscar.psi_des);

%% Rotate Current Position from WF to BF (capteurs)
[Oscar.X_BF,Oscar.Y_BF,Oscar.Z_BF] = RotateWFtoBF(x,y,z,phi,theta,psi);

%% Rotate Current Velocity from WF to BF
[Oscar.X_BF_dot,Oscar.Y_BF_dot,Oscar.Z_BF_dot] = RotateWFtoBF(Oscar.X_dot,Oscar.Y_dot,Oscar.Z_dot,phi,theta,psi);

%% X Position PID controller 
x_error = Oscar.X_des_BF - Oscar.X_BF;
if(abs(x_error) < Oscar.X_KI_lim)
    x_error_sum = x_error_sum + x_error; % donc augmente les oscillations 
end
cp = Oscar.X_KP*x_error; %P
ci = Oscar.X_KI*Oscar.Ts*x_error_sum; %I
ci = min(Oscar.theta_max, max(-Oscar.theta_max, ci)); %Saturate ci (theta limit� � 45deg)
cd = Oscar.X_KD*Oscar.X_BF_dot; %D
Oscar.theta_des =  - (cp + ci + cd); %Theta and X inversely related
Oscar.theta_des = min(Oscar.theta_max, max(-Oscar.theta_max, Oscar.theta_des));


%% Y Position PID controller
y_error = Oscar.Y_des_BF - Oscar.Y_BF;
if(abs(y_error) < Oscar.Y_KI_lim)
    y_error_sum = y_error_sum + y_error;
end
cp = Oscar.Y_KP*y_error; %P
ci = Oscar.Y_KI*Oscar.Ts*y_error_sum; %I
ci = min(Oscar.phi_max, max(-Oscar.phi_max, ci)); %Saturate ci
cd = Oscar.Y_KD*Oscar.Y_BF_dot; %D
Oscar.phi_des = cp + ci + cd; 
Oscar.phi_des = min(Oscar.phi_max, max(-Oscar.phi_max, Oscar.phi_des));

end