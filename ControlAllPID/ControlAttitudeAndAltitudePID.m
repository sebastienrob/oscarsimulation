% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% ----!---- CONTROL STEP 2 ----!----
% ----!---- MIDDLE RATE FREQUENCE ----!----
% ----!---- IMU sensor ----!----

% This function implements a PID Controller.
% use Zieger or other method for identification parameters

% A LOW LEVEL controller takes desired roll and pitch angles for inputs and 
% controls the error between the desired and actual Euler angles. 
% After the control, inputs_Sys(pd, qd, rd) are calculated.

function ControlAttitudeAndAltitudePID

persistent z_error_sum;
persistent phi_error_sum;
persistent theta_error_sum;
persistent psi_error_sum;

global Oscar

%% initialize persistent variables at beginning of simulation
if Oscar.init==0
    z_error_sum = 0;
    phi_error_sum = 0;
    theta_error_sum = 0;
    psi_error_sum = 0;
end

%% DONNES CAPTEUR (ICI M.D.)

phi = Oscar.phi;
theta = Oscar.theta;
psi = Oscar.psi;

%% ----!---- Z Position PID Controller/Altitude Controller ----!----

z_error = Oscar.Z_des_BF-Oscar.Z_BF;
if(abs(z_error) < Oscar.Z_KI_lim)
    z_error_sum = z_error_sum + z_error;
end
cp = Oscar.Z_KP*z_error;         %Proportional term
ci = Oscar.Z_KI*Oscar.Ts*z_error_sum; %Integral term
ci = min(Oscar.U1_max, max(Oscar.U1_min, ci));    %Saturate ci
cd = Oscar.Z_KD*Oscar.Z_dot;                  %Derivative term
Oscar.U1 = -(cp + ci + cd)/(cos(theta)*cos(phi)) + (Oscar.m * Oscar.g)/(cos(theta)*cos(phi));   %Negative since Thurst and Z inversely related
Oscar.U1 = min(Oscar.U1_max, max(Oscar.U1_min, Oscar.U1));


%% ----!---- Attitude Controller ----!----

%% Roll PID Controller
phi_error = Oscar.phi_des - phi;
if(abs(phi_error) < Oscar.phi_KI_lim)
    phi_error_sum = phi_error_sum + phi_error;
end
cp = Oscar.phi_KP*phi_error;
ci = Oscar.phi_KI*Oscar.Ts*phi_error_sum;
ci = min(Oscar.p_max, max(-Oscar.p_max, ci));
cd = Oscar.phi_KD*Oscar.p;
Oscar.p_des = cp + ci + cd;
Oscar.p_des = min(Oscar.p_max, max(-Oscar.p_max, Oscar.p_des));

%% Pitch PID Controller
theta_error = Oscar.theta_des - theta;
if(abs(theta_error) < Oscar.theta_KI_lim)
    theta_error_sum = theta_error_sum + theta_error;
end
cp = Oscar.theta_KP*theta_error;
ci = Oscar.theta_KI*Oscar.Ts*theta_error_sum;
ci = min(Oscar.q_max, max(-Oscar.q_max, ci));
cd = Oscar.theta_KD*Oscar.q;
Oscar.q_des = cp + ci + cd;
Oscar.q_des = min(Oscar.q_max, max(-Oscar.q_max, Oscar.q_des));

%% Yaw PID Controller

psi_error = Oscar.psi_des - psi;
if(abs(psi_error) < Oscar.psi_KI_lim)
    psi_error_sum = psi_error_sum + psi_error;
end
cp = Oscar.psi_KP*psi_error;
ci = Oscar.psi_KI*Oscar.Ts*psi_error_sum;
ci = min(Oscar.r_max, max(-Oscar.r_max, ci));
cd = Oscar.psi_KD*Oscar.r;
Oscar.r_des = cp + ci + cd;
Oscar.r_des = min(Oscar.r_max, max(-Oscar.r_max, Oscar.r_des));

end