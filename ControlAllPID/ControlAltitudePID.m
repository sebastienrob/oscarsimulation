% Sbastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% ----!---- CONTROL STEP 2.2 ----!----
% ----!---- MIDDLE RATE FREQUENCE ----!----
% ----!---- IMU sensor ----!----

% This function implements a PID Controller.
% use Zieger or other method for identification parameters

% A HIGH LEVEL controller outputs desired thrust based on 
% errors between the World and desired Z positions.
% N.B : Z's control is decoupling with others variables in MD.

function ControlAltitudePID

persistent z_error_sum;
global Oscar

if Oscar.init==0
    z_error_sum = 0;
end

%% DONNES CAPTEUR (ICI M.D.)
phi = Oscar.phi;
theta = Oscar.theta;

z_error = Oscar.Z_des_BF-Oscar.Z_BF;
if(abs(z_error) < Oscar.Z_KI_lim)
    z_error_sum = z_error_sum + z_error;
end

cp = Oscar.Z_KP*z_error;         %Proportional term
ci = Oscar.Z_KI*Oscar.Ts*z_error_sum; %Integral term
ci = min(Oscar.U1_max, max(Oscar.U1_min, ci));    %Saturate ci
cd = Oscar.Z_KD*Oscar.Z_dot;                  %Derivative term
Oscar.U1 = -(cp + ci + cd)/(cos(theta)*cos(phi)) + (Oscar.m * Oscar.g)/(cos(theta)*cos(phi));   %Negative since Thurst and Z inversely related
Oscar.U1 = min(Oscar.U1_max, max(Oscar.U1_min, Oscar.U1));

end
