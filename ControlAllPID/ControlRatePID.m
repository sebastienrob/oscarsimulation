% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% ----!---- CONTROL STEP 3 ----!----
% ----!---- HIGH RATE FREQUENCE ----!----
% ----!---- IMU-gyroscope(BF) sensor ----!----

% This function implements a PID Controller.
% use Zieger or other method for identification parameters

% A LOWEST LEVEL controller takes desired angulars rates from the attitude controller for inputs and 
% controls the error between the desired and actual rate Euler angles of Body Frame. 
% After the control, The outputs are then sent directly to the motors.

function ControlRatePID

persistent p_error_sum;
persistent q_error_sum;
persistent r_error_sum;

global Oscar

%% initialize persistent variables at beginning of simulation
if Oscar.init==0
    p_error_sum = 0;
    q_error_sum = 0;
    r_error_sum = 0;
end

%% DONNES CAPTEUR (ICI M.D.)

p = Oscar.p;
q = Oscar.q;
r = Oscar.r;

%% Angular Rate Controller

%% Roll rate PID Controller
p_error = Oscar.p_des - p;
if(abs(p_error) < Oscar.p_KI_lim)
    p_error_sum = p_error_sum + p_error;
end
cp = Oscar.p_KP*p_error;
ci = Oscar.p_KI*Oscar.Ts*p_error_sum;
ci = min(Oscar.U2_max, max(Oscar.U2_min, ci));
cd = Oscar.p_KD*Oscar.p_dot;
Oscar.U2 = cp + ci + cd;
Oscar.U2 = min(Oscar.U2_max, max(Oscar.U2_min, Oscar.U2));

%% Pitch rate PID Controller
q_error = Oscar.q_des - q;
if(abs(q_error) < Oscar.q_KI_lim)
    q_error_sum = q_error_sum + q_error;
end
cp = Oscar.q_KP*q_error;
ci = Oscar.q_KI*Oscar.Ts*q_error_sum;
ci = min(Oscar.U3_max, max(Oscar.U3_min, ci));
cd = Oscar.q_KD*Oscar.q_dot;
Oscar.U3 = cp + ci + cd;
Oscar.U3 = min(Oscar.U3_max, max(Oscar.U3_min, Oscar.U3));

%% Yaw rate PID Controller
r_error = Oscar.r_des - r;
if(abs(r_error) < Oscar.r_KI_lim)
    r_error_sum = r_error_sum + r_error;
end
cp = Oscar.r_KP*r_error;
ci = Oscar.r_KI*Oscar.Ts*r_error_sum;
ci = min(Oscar.U4_max, max(Oscar.U4_min, ci));
cd = Oscar.r_KD*Oscar.r_dot;
Oscar.U4 = cp + ci + cd;
Oscar.U4 = min(Oscar.U4_max, max(Oscar.U4_min, Oscar.U4));

end