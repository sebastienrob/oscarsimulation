% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% This function converts the desired force and moment control inputs into the desired speed of the motors. 
% These speeds are then limited by the physical properties of our motor. 
% The conventional control commands are then re-computed with the limited motor speeds for input into the control system.
% Note that these motor speeds would be the signals sent to the ESCs on the actual quadrotor.
% the desired motor speeds are calculated.

% N.B : it's for "+" quadrotor orientation, not "X" configuration

function QuadMotorSpeed
global Oscar

%% Calculate motor speeds (rad/s)^2
w1 = Oscar.U1/(4*Oscar.KT) + Oscar.U3/(2*Oscar.KT*Oscar.l) + Oscar.U4/(4*Oscar.Kd);
w2 = Oscar.U1/(4*Oscar.KT) - Oscar.U2/(2*Oscar.KT*Oscar.l) - Oscar.U4/(4*Oscar.Kd);
w3 = Oscar.U1/(4*Oscar.KT) - Oscar.U3/(2*Oscar.KT*Oscar.l) + Oscar.U4/(4*Oscar.Kd);
w4 = Oscar.U1/(4*Oscar.KT) + Oscar.U2/(2*Oscar.KT*Oscar.l) - Oscar.U4/(4*Oscar.Kd);

%% Apply realistic motor speed limits

if w1 > Oscar.max_motor_speed^2
    w1 = Oscar.max_motor_speed^2;
end
if w1 < Oscar.min_motor_speed^2
    w1 = Oscar.min_motor_speed^2;
end

if w2 > Oscar.max_motor_speed^2
    w2 = Oscar.max_motor_speed^2;
end
if w2 < Oscar.min_motor_speed^2
    w2 = Oscar.min_motor_speed^2;
end

if w3 > Oscar.max_motor_speed^2
    w3 = Oscar.max_motor_speed^2;
end
if w3 < Oscar.min_motor_speed^2
    w3 = Oscar.min_motor_speed^2;
end

if w4 > Oscar.max_motor_speed^2
    w4 = Oscar.max_motor_speed^2;
end
if w4 < Oscar.min_motor_speed^2
    w4 = Oscar.min_motor_speed^2;
end

Oscar.O1 = sqrt(w1);    % Front M moteur speed
Oscar.O2 = sqrt(w2);    % Right M moteur speed
Oscar.O3 = sqrt(w3);    % Rear M moteur speed
Oscar.O4 = sqrt(w4);    % Left M moteur speed

Oscar.O1_plot(Oscar.counter) = Oscar.O1;
Oscar.O2_plot(Oscar.counter) = Oscar.O2;
Oscar.O3_plot(Oscar.counter) = Oscar.O3;
Oscar.O4_plot(Oscar.counter) = Oscar.O4;

%% Re-compute traditional control inputs

Oscar.U1 = Oscar.KT*(Oscar.O1^2 + Oscar.O2^2 + Oscar.O3^2 + Oscar.O4^2);
Oscar.U1_plot(Oscar.counter) = Oscar.U1;
  
Oscar.U2 = Oscar.KT*Oscar.l*(Oscar.O4^2 - Oscar.O2^2);
Oscar.U2_plot(Oscar.counter) = Oscar.U2;
  
Oscar.U3 = Oscar.KT*Oscar.l*(Oscar.O1^2 - Oscar.O3^2);
Oscar.U3_plot(Oscar.counter) = Oscar.U3;
  
Oscar.U4 = Oscar.Kd*(Oscar.O1^2 + Oscar.O3^2 - Oscar.O2^2 - Oscar.O4^2);
Oscar.U4_plot(Oscar.counter) = Oscar.U4;
  
Oscar.O = (Oscar.O1 - Oscar.O2 + Oscar.O3 - Oscar.O4);
Oscar.O_plot(Oscar.counter) = Oscar.O;

end