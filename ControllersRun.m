% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

%% Position Control
if(Oscar.PositionPID)
    ControlPositionPID;
end
if(Oscar.PositionLMPC)
    ControlPositionLMPC;
end

%% Altitude Control
if(Oscar.AltitudePID)
ControlAltitudePID;
end
if(Oscar.AltitudeLMPC)
ControlAltitudeLMPC;
end

%% Altitude Control
if(Oscar.AttitudePID)
ControlAttitudePID;
end
if(Oscar.AttitudeLMPC)
ControlAttitudeLMPC;
end

%% Motor/Rate Control
if(Oscar.MotorsRatePID)
ControlMotorsRatePID;
end

    
