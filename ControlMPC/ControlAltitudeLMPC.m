% Sébastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com
% https://osqp.org/docs/citing/index.html for OSQP solver
% http://downloads.hindawi.com/journals/aaa/2015/916864.pdf

% ----!---- CONTROL STEP 2.1 ----!----
% ----!---- MIDDLE RATE FREQUENCE ----!----
% ----!---- Camera-SLAM(BF)/IMU-odometry(BF)/GPS(WF) or others sensors ----!----

% This function implements a LMPC Controller.
% need to choice horizon T and good constraints. 

% A HIGH LEVEL controller outputs desired thrust based on 
% errors between the World and desired Z positions.
% N.B : Z's control is decoupling with others variables in MD.

function ControlAltitudeLMPC

global Oscar;

%% Discrete time model of altitude quadrorotor
Ad = [1 Oscar.Ts ; 0 1];
Bd = [0 ; Oscar.Ts];
[nx, nu] = size(Bd); % ici taille nx = 2 ligne et nu = 1 colonnes

%% Objective function
Q = diag([10 46]); % height on position z 47 vitesse circle
QN = Q;
R =0.1*eye(1); % G = g - U1/m // U1 = m(g-G) 

%% ever computed in ControlPositionMPC
% Initial and reference states (in World frame)
%% Initial and reference states (in World frame)
phi = Oscar.phi;
theta = Oscar.theta;
psi = Oscar.psi;
% Rotate Desired Position from WF to BF (Z axis rotation only?)
% Rotate Current Position from WF to BF (capteurs)
% Rotate Current Velocity from WF to BF
%% Rotate Desired Velocity from WF to BF
[Oscar.X_BF_dot,Oscar.Y_BF_dot,Oscar.Z_BF_dot] = RotateWFtoBF(Oscar.X_dot,Oscar.Y_dot,Oscar.Z_dot,phi,theta,psi);

%% initial state and desired state
x0 = [Oscar.Z_BF; Oscar.Z_BF_dot];
xr = [Oscar.Z_des_BF; Oscar.Z_des_dot_BF];

%% Constraints input
umin = [Oscar.G_max]; 
umax = [Oscar.G_min]; % G constrainst (tranform to obtain thrust U1)

%% constraint translation
xmin = [-Inf; -Inf]; % pose constrainst (z, vz)
xmax = [ Inf;  Inf];

%% Prediction horizon
N = 100;

%% Cast MPC problem to a QP: x = (x(0),x(1),...,x(N),u(0),...,u(N-1)) ????
% - quadratic objective
P = blkdiag( kron(speye(N), Q), QN, kron(speye(N), R) );
% - linear objective
q = [repmat(-Q*xr, N, 1); -QN*xr; zeros(N*nu, 1)];
% - linear dynamics
Ax = kron(speye(N+1), -speye(nx)) + kron(sparse(diag(ones(N, 1), -1)), Ad);
Bu = kron([sparse(1, N); speye(N)], Bd);
Aeq = [Ax, Bu];
leq = [-x0; zeros(N*nx, 1)];
ueq = leq;
% - input and state constraints
Aineq = speye((N+1)*nx + N*nu);
lineq = [repmat(xmin, N+1, 1); repmat(umin, N, 1)];
uineq = [repmat(xmax, N+1, 1); repmat(umax, N, 1)];
% - OSQP constraints
A = [Aeq; Aineq];
l = [leq; lineq];
u = [ueq; uineq];

%% Create an OSQP object (initialize solver)
prob = osqp;

%% Setup workspace (specification of problem)
% q, l et u are arrays. P and A are sparses matrix (OSQP use only the upper
% triangular part and A is full
% parameter verbose is false for dont' print information on terminal
prob.setup(P, q, A, l, u, 'warm_start', true,'verbose',false);
    
%% Solve
res = prob.solve();

%% Check solver status
if ~strcmp(res.info.status, 'solved')
    error('OSQP did not solve the problem!')
end

%% Apply first control input to the plant
ctrl = res.x((N+1)*nx+1:(N+1)*nx+nu);
Oscar.U1 = Oscar.m * (Oscar.g - ctrl(1));
Oscar.U1 = -ctrl(1)/(cos(theta)*cos(phi)) + (Oscar.m * Oscar.g)/(cos(theta)*cos(phi));

end