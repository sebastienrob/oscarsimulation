% Sébastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com
% https://osqp.org/docs/citing/index.html for OSQP solver
% http://downloads.hindawi.com/journals/aaa/2015/916864.pdf

% ----!---- CONTROL STEP 2.2 ----!----
% ----!---- MIDDLE RATE FREQUENCE ----!----
% ----!---- IMU sensor ----!----

% This function implements a LMPC Controller.
% need to choice horizon T and good constraints. 

% A LOW LEVEL controller takes desired roll and pitch angles for inputs and 
% controls the error between the desired and actual Euler angles. 
% After the control, inputs_Sys(pd, qd, rd) are calculated.

function ControlAttitudeLMPC

global Oscar;
 
%% Discrete time model of position quadrorotor
Ad = [1 Oscar.Ts 0 0 0 0 ; 0 1 0 0 0 0 ; 0 0 1 Oscar.Ts 0 0 ; 0 0 0 1 0 0 ; 0 0 0 0 1 Oscar.Ts ; 0 0 0 0 0 1];
Bd = [0 0 0 ; (1/Oscar.Jx)*Oscar.Ts 0 0 ; 0 0 0 ; 0 (1/Oscar.Jy)*Oscar.Ts 0 ; 0 0 0 ; 0 0 (1/Oscar.Jz)*Oscar.Ts];
[nx, nu] = size(Bd); % ici taille nx = 6 ligne et nu = 3 colonnes

%% Objective function
Q = diag([10 0 10 0 10 0]); % height on position x and y
QN = Q;
R =0.1*eye(3);

%% Initial and reference states (in World frame)
% DONNES CAPTEUR (ICI M.D.)
% linear model (hyp. littple angle) so velocity angle in BF is the same in WF

%% Calcul velocity desired here (after move in trajectory generation NO control)
time = Oscar.t_plot(Oscar.counter); 
Oscar.theta_des_dot = (Oscar.theta_des - Oscar.theta_des_precedent) / (time - (time - Oscar.Ts));
Oscar.phi_des_dot = (Oscar.phi_des - Oscar.phi_des_precedent) / (time - (time - Oscar.Ts));
Oscar.psi_des_dot = (Oscar.psi_des - Oscar.psi_des_precedent) / (time - (time - Oscar.Ts));
Oscar.theta_des_precedent = Oscar.theta_des;
Oscar.phi_des_precedent = Oscar.phi_des;
Oscar.psi_des_precedent = Oscar.psi_des;

%% initial state and desired state
x0 = [Oscar.theta ; Oscar.theta_dot ; Oscar.phi ; Oscar.phi_dot ; Oscar.psi ; Oscar.psi_dot];
xr = [Oscar.theta_des; Oscar.theta_des_dot ; Oscar.phi_des ; Oscar.phi_des_dot ; Oscar.psi_des ; Oscar.psi_des_dot];

%% Constraints input (roll and pitch)
umin = [Oscar.U2_min; Oscar.U3_min ; Oscar.U4_min]; % roll/pitch/yaw commands constraints
umax = [Oscar.U2_max ; Oscar.U3_max ; Oscar.U4_max]; 

%% constraint translation
xmin = [-Oscar.theta_max; -Inf; -Oscar.phi_max ; -Inf ; -inf ; -inf]; % pose constrainst (pitch, Vpitch, roll, Vroll, yaw, Vyaw)
xmax = [Oscar.theta_max ; Inf ; Oscar.phi_max ; Inf ; inf ; Inf];

%% Prediction horizon
N = 100;

%% Cast MPC problem to a QP: x = (x(0),x(1),...,x(N),u(0),...,u(N-1)) ????
% - quadratic objective
P = blkdiag( kron(speye(N), Q), QN, kron(speye(N), R) );
% - linear objective
q = [repmat(-Q*xr, N, 1); -QN*xr; zeros(N*nu, 1)];
% - linear dynamics
Ax = kron(speye(N+1), -speye(nx)) + kron(sparse(diag(ones(N, 1), -1)), Ad);
Bu = kron([sparse(1, N); speye(N)], Bd);
Aeq = [Ax, Bu];
leq = [-x0; zeros(N*nx, 1)];
ueq = leq;
% - input and state constraints
Aineq = speye((N+1)*nx + N*nu);
lineq = [repmat(xmin, N+1, 1); repmat(umin, N, 1)];
uineq = [repmat(xmax, N+1, 1); repmat(umax, N, 1)];
% - OSQP constraints
A = [Aeq; Aineq];
l = [leq; lineq];
u = [ueq; uineq];

%% Create an OSQP object (initialize solver)
prob = osqp;

%% Setup workspace (specification of problem)
% q, l et u are arrays. P and A are sparses matrix (OSQP use only the upper
% triangular part and A is full
% parameter verbose is false for dont' print information on terminal
prob.setup(P, q, A, l, u, 'warm_start', true,'verbose',false);
    
%% Solve
res = prob.solve();

%% Check solver status
if ~strcmp(res.info.status, 'solved')
    error('OSQP did not solve the problem!')
end

%% Apply first control input to the plant
ctrl = res.x((N+1)*nx+1:(N+1)*nx+nu);
Oscar.p_des = ctrl(2);
Oscar.q_des = ctrl(1);
Oscar.r_des = ctrl(3);
end