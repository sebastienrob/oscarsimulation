% Sébastien Petit 
% Montpellier, France
% email : sebastien.petit.rob@gmail.com
% https://osqp.org/docs/citing/index.html for OSQP solver
% http://downloads.hindawi.com/journals/aaa/2015/916864.pdf

% ----!---- CONTROL STEP 1.1 ----!----
% ----!---- LOW RATE FREQUENCE ----!----
% ----!---- Camera-SLAM(BF)/IMU-odometry(BF)/GPS(WF) or others sensors ----!----

% This function implements a LMPC Controller.
% need to choice horizon T and good constraints. 

% A HIGH LEVEL controller outputs desired roll and pitch angles based on 
% errors between the World and desired X and Y positions.

function ControlPositionLMPC

global Oscar;

%% Discrete time model of position quadrorotor
Ad = [1 Oscar.Ts 0 0 ; 0 1 0 0 ; 0 0 1 Oscar.Ts ; 0 0 0 1];
Bd = [0 0 ; -Oscar.g*Oscar.Ts 0 ; 0 0 ; 0 Oscar.g*Oscar.Ts ];
[nx, nu] = size(Bd); % ici taille nx = 4 ligne et nu = 2 colonnes

%% Objective function
Q = diag([10 0 10 0]); % height on position x and y
QN = Q;
R =40*eye(2);

%% Initial and reference states (in World frame)
x = Oscar.X;
y = Oscar.Y;
z = Oscar.Z;
phi = Oscar.phi;
theta = Oscar.theta;
psi = Oscar.psi;

%% Rotate Desired Position from WF to BF (Z axis rotation only?)
[Oscar.X_des_BF,Oscar.Y_des_BF,Oscar.Z_des_BF] = RotateWFtoBF(Oscar.X_des,Oscar.Y_des,Oscar.Z_des,0*Oscar.phi_des,0*Oscar.theta_des,Oscar.psi_des);

%% Rotate Current Position from WF to BF (capteurs)
[Oscar.X_BF,Oscar.Y_BF,Oscar.Z_BF] = RotateWFtoBF(x,y,z,phi,theta,psi);

%% Rotate Current Velocity from WF to BF
[Oscar.X_BF_dot,Oscar.Y_BF_dot,Oscar.Z_BF_dot] = RotateWFtoBF(Oscar.X_dot,Oscar.Y_dot,Oscar.Z_dot,phi,theta,psi);

%% Rotate Desired Velocity from WF to BF
[Oscar.X_des_dot_BF,Oscar.Y_des_dot_BF,Oscar.Z_des_dot_BF] = RotateWFtoBF(Oscar.X_des_dot,Oscar.Y_des_dot,Oscar.Z_des_dot,0*Oscar.phi_des,0*Oscar.theta_des,Oscar.psi_des);

%% initial state and desired state
x0 = [Oscar.X_BF; Oscar.X_BF_dot; Oscar.Y_BF; Oscar.Y_BF_dot];
xr = [Oscar.X_des_BF; Oscar.X_des_dot_BF; Oscar.Y_des_BF; Oscar.Y_des_dot_BF];

%% Constraints input (roll and pitch)
umin = [-Oscar.phi_max; -Oscar.theta_max]; % roll and pitch constraint
umax = [Oscar.phi_max; Oscar.theta_max];

%% constraint translation
xmin = [-Inf; -Inf; -Inf; -Inf]; % pose constrainst (x, vx, y, vy)
xmax = [ Inf;  Inf;  Inf; Inf];

%% Prediction horizon
N = 100;

%% Cast MPC problem to a QP: x = (x(0),x(1),...,x(N),u(0),...,u(N-1)) ????
% - quadratic objective
P = blkdiag( kron(speye(N), Q), QN, kron(speye(N), R) );
% - linear objective
q = [repmat(-Q*xr, N, 1); -QN*xr; zeros(N*nu, 1)];
% - linear dynamics
Ax = kron(speye(N+1), -speye(nx)) + kron(sparse(diag(ones(N, 1), -1)), Ad);
Bu = kron([sparse(1, N); speye(N)], Bd);
Aeq = [Ax, Bu];
leq = [-x0; zeros(N*nx, 1)];
ueq = leq;
% - input and state constraints
Aineq = speye((N+1)*nx + N*nu);
lineq = [repmat(xmin, N+1, 1); repmat(umin, N, 1)];
uineq = [repmat(xmax, N+1, 1); repmat(umax, N, 1)];
% - OSQP constraints
A = [Aeq; Aineq];
l = [leq; lineq];
u = [ueq; uineq];

%% Create an OSQP object (initialize solver)
prob = osqp;

%% Setup workspace (specification of problem)
% q, l et u are arrays. P and A are sparses matrix (OSQP use only the upper
% triangular part and A is full
% parameter verbose is false for dont' print information on terminal
prob.setup(P, q, A, l, u, 'warm_start', true,'verbose',false);
    
%% Solve
res = prob.solve();

%% Check solver status
if ~strcmp(res.info.status, 'solved')
    error('OSQP did not solve the problem!')
end

%% Apply first control input to the plant
ctrl = res.x((N+1)*nx+1:(N+1)*nx+nu);
    
Oscar.theta_des = ctrl(1);
Oscar.phi_des = ctrl(2);

end