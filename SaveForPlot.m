% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% Save pose and angle for plot.

function SaveForPlot
global Oscar;

%% MAJ Plotting Variables

% Flip positive Z axis up for intuitive plotting
Oscar.Z_plot(Oscar.counter) = -Oscar.Z;
Oscar.Z_des_plot(Oscar.counter) = -Oscar.Z_des;

Oscar.X_plot(Oscar.counter) = Oscar.X;
Oscar.X_des_plot(Oscar.counter) = Oscar.X_des;

Oscar.Y_plot(Oscar.counter) = Oscar.Y;
Oscar.Y_des_plot(Oscar.counter) = Oscar.Y_des;

Oscar.phi_plot(Oscar.counter) = Oscar.phi;
Oscar.phi_ref_plot(Oscar.counter) = Oscar.phi_des;

Oscar.theta_plot(Oscar.counter) = Oscar.theta;
Oscar.theta_ref_plot(Oscar.counter) = Oscar.theta_des;

Oscar.psi_plot(Oscar.counter) = Oscar.psi;
Oscar.psi_ref_plot(Oscar.counter) = Oscar.psi_des;

Oscar.X_ddot_meas_plot(Oscar.counter) = Oscar.X_ddot;
Oscar.Y_ddot_meas_plot(Oscar.counter) = Oscar.Y_ddot;
Oscar.Z_ddot_meas_plot(Oscar.counter) = Oscar.Z_ddot;

if Oscar.init==0
Oscar.U1_plot(Oscar.counter) = Oscar.U1;
Oscar.U2_plot(Oscar.counter) = Oscar.U2;
Oscar.U3_plot(Oscar.counter) = Oscar.U3;
Oscar.U4_plot(Oscar.counter) = Oscar.U4;
Oscar.O_plot(Oscar.counter) = Oscar.Obar;
Oscar.X_ddot_meas_plot(Oscar.counter) = Oscar.X_ddot;
Oscar.Y_ddot_meas_plot(Oscar.counter) = Oscar.Y_ddot;
Oscar.Z_ddot_meas_plot(Oscar.counter) = Oscar.Z_ddot;
end

Oscar.X_des_dot_plot(Oscar.counter) = Oscar.X_des_dot;
Oscar.X_dot_plot(Oscar.counter) = Oscar.X_dot;

Oscar.Y_des_dot_plot(Oscar.counter) = Oscar.Y_des_dot;
Oscar.Y_dot_plot(Oscar.counter) = Oscar.Y_dot;

Oscar.Z_des_dot_plot(Oscar.counter) = Oscar.Z_des_dot;
Oscar.Z_dot_plot(Oscar.counter) = Oscar.Z_dot;

end