% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this script compute coeff for simple trajectory generation
% stabilisation 3s

global Oscar

x0 = [Oscar.X ; Oscar.Y ; Oscar.Z];
xf = [Oscar.Xf ; Oscar.Yf ; Oscar.Zf];

a = -2*(xf(1:3) - x0(1:3))/(max(Oscar.t_plot)-Oscar.DurationWayPoint)^3;
b = 3 * (xf(1:3) - x0(1:3))/(max(Oscar.t_plot)-Oscar.DurationWayPoint)^2;
c = zeros(3,1);
d = x0(1:3);
Oscar.CoeffTraj = [a b c d]; 



