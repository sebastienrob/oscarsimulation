% Sébastien Petit
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% here it's programm to compute choice of trajectory for users

%% MAJ desired position (Xd,Yd,Zd POLYNOME) --- need yaw angle desired
if(Oscar.PolynomeTrajectory)
if ( Oscar.t_plot(Oscar.counter) < max(Oscar.t_plot) - Oscar.DurationWayPoint) % stabilization around stable point
Polynome
end
end
    
%% MAJ desired position (Xd,Yd,Zd LEMNISCALE) --- need yaw angle desired
if(Oscar.LemniscapeTrajectory)
Lemniscape
end

%% MAJ desired position (Xd,Yd,Zd LEMNISCALE) --- need yaw angle desired
if(Oscar.CircleTrajectory)
Circle
end

%% MAJ desired position (Xd,Yd,Zd TOURNADE) --- need yaw angle desired
if(Oscar.TournadeTrajectory)
Tournade
end