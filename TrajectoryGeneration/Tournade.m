% Sébastien Petit
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this script MAJ desired  X,Y,Z  - trajectory generation lemniscate

function tournade

global Oscar;
time = Oscar.t_plot(Oscar.counter);

z = 0:Oscar.Ts:Oscar.sim_time;
y = sin(2*z);
x = cos(2*z);

Oscar.X_des = x(Oscar.counter-1);
Oscar.X_des = y(Oscar.counter-1);
Oscar.X_des = z(Oscar.counter-1);

Oscar.X_des_dot = (Oscar.X_des - Oscar.X_des_precedent) / (time - (time - Oscar.Ts));
Oscar.Y_des_dot = (Oscar.Y_des - Oscar.Y_des_precedent) / (time - (time - Oscar.Ts));
Oscar.Z_des_dot = (Oscar.Z_des - Oscar.Z_des_precedent) / (time - (time - Oscar.Ts));

Oscar.X_des_precedent = Oscar.X_des;
Oscar.Y_des_precedent = Oscar.Y_des;
Oscar.Z_des_precedent = Oscar.Z_des;
end