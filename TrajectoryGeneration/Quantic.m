% Sébastien Petit
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this script MAJ desired  X,Y,Z  - trajectory generation Quantic


function Quantic

global Oscar;

wpts = [0 1 2 3; 0 1 -1 -3 ; 0 1 -1 -4];
tpts = 0:Oscar.sim_time;
tvec = Oscar.t_plot;
[q, qd, qdd, pp] = quinticpolytraj(wpts, tpts, tvec);

plot(tvec, q)
hold all
plot(tpts, wpts, 'x')
xlabel('t')
ylabel('Positions')
legend('X-positions','Y-positions')
hold off

end