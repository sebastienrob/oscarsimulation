% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this script MAJ desired  X,Y,Z  - trajectory generation

global Oscar;

time = Oscar.t_plot(Oscar.counter); 

Oscar.X_des = Oscar.CoeffTraj(1,4) + Oscar.CoeffTraj(1,3)*time + Oscar.CoeffTraj(1,2)*time^2 + Oscar.CoeffTraj(1,1)*time^3;
Oscar.Y_des = Oscar.CoeffTraj(2,4) + Oscar.CoeffTraj(2,3)*time + Oscar.CoeffTraj(2,2)*time^2 + Oscar.CoeffTraj(2,1)*time^3;
Oscar.Z_des = Oscar.CoeffTraj(3,4) + Oscar.CoeffTraj(3,3)*time + Oscar.CoeffTraj(3,2)*time^2 + Oscar.CoeffTraj(3,1)*time^3;