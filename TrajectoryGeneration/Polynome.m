% Sébastien Petit
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this script MAJ desired  X,Y,Z  - trajectory generation Polynome

function Polynome

global Oscar;

%% Compute Trajectory coeff.
if (Oscar.init==0)
    x0 = [Oscar.X ; Oscar.Y ; Oscar.Z];
    xf = [Oscar.Xf ; Oscar.Yf ; Oscar.Zf];

    a = -2*(xf(1:3) - x0(1:3))/(max(Oscar.t_plot)-Oscar.DurationWayPoint)^3;
    b = 3 * (xf(1:3) - x0(1:3))/(max(Oscar.t_plot)-Oscar.DurationWayPoint)^2;
    c = zeros(3,1);
    d = x0(1:3);
    Oscar.CoeffTraj = [a b c d]; 
end

time = Oscar.t_plot(Oscar.counter); 

Oscar.X_des = Oscar.CoeffTraj(1,4) + Oscar.CoeffTraj(1,3)*time + Oscar.CoeffTraj(1,2)*time^2 + Oscar.CoeffTraj(1,1)*time^3;
Oscar.Y_des = Oscar.CoeffTraj(2,4) + Oscar.CoeffTraj(2,3)*time + Oscar.CoeffTraj(2,2)*time^2 + Oscar.CoeffTraj(2,1)*time^3;
Oscar.Z_des = Oscar.CoeffTraj(3,4) + Oscar.CoeffTraj(3,3)*time + Oscar.CoeffTraj(3,2)*time^2 + Oscar.CoeffTraj(3,1)*time^3;

Oscar.X_des_dot = (Oscar.X_des - Oscar.X_des_precedent) / (time - (time - Oscar.Ts));
Oscar.Y_des_dot = (Oscar.Y_des - Oscar.Y_des_precedent) / (time - (time - Oscar.Ts));
Oscar.Z_des_dot = (Oscar.Z_des - Oscar.Z_des_precedent) / (time - (time - Oscar.Ts));
Oscar.X_des_precedent = Oscar.X_des;
Oscar.Y_des_precedent = Oscar.Y_des;
Oscar.Z_des_precedent = Oscar.Z_des;

end