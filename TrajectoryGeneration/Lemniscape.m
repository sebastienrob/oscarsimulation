% Sébastien Petit
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% this script MAJ desired  X,Y,Z  - trajectory generation lemniscate

function Lemniscape

global Oscar;

global lemniscape;

time = Oscar.t_plot(Oscar.counter);
lemniscape.r = 1;
lemniscape.alpha = 1;

Oscar.X_des = lemniscape.r*cos(lemniscape.alpha*time)*sin(lemniscape.alpha*time);
Oscar.Y_des = lemniscape.r*cos(lemniscape.alpha*time);
Oscar.Z_des = 0;

Oscar.X_des_dot = (Oscar.X_des - Oscar.X_des_precedent) / (time - (time - Oscar.Ts));
Oscar.Y_des_dot = (Oscar.Y_des - Oscar.Y_des_precedent) / (time - (time - Oscar.Ts));
Oscar.Z_des_dot = (Oscar.Z_des - Oscar.Z_des_precedent) / (time - (time - Oscar.Ts));

Oscar.X_des_precedent = Oscar.X_des;
Oscar.Y_des_precedent = Oscar.Y_des;
Oscar.Z_des_precedent = Oscar.Z_des;

end