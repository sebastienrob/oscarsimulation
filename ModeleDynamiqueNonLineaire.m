% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% Equations of motions (dynamic).
% equations are both non-linear and highly coupled.

function ModeleDynamiqueNonLineaire
global Oscar;

%% MAJ Accelerations in the world frame

Oscar.X_ddot = (-(cos(Oscar.phi)*sin(Oscar.theta)*cos(Oscar.psi)+sin(Oscar.phi)*sin(Oscar.psi))*Oscar.U1-Oscar.Kdx*Oscar.X_dot)/Oscar.m;
Oscar.Y_ddot = (-(cos(Oscar.phi)*sin(Oscar.psi)*sin(Oscar.theta)-cos(Oscar.psi)*sin(Oscar.phi))*Oscar.U1-Oscar.Kdy*Oscar.Y_dot)/Oscar.m;
Oscar.Z_ddot = (-(cos(Oscar.phi)*cos(Oscar.theta))*Oscar.U1-Oscar.Kdz*Oscar.Z_dot)/Oscar.m+Oscar.g;

%% MAJ angular accelerations in the body frame

Oscar.p_dot = (Oscar.q*Oscar.r*(Oscar.Jy - Oscar.Jz) - Oscar.Jr*Oscar.q*Oscar.Obar + Oscar.l*Oscar.U2)/Oscar.Jx;
Oscar.q_dot = (Oscar.p*Oscar.r*(Oscar.Jz - Oscar.Jx) + Oscar.Jr*Oscar.p*Oscar.Obar + Oscar.l*Oscar.U3)/Oscar.Jy;
Oscar.r_dot = (Oscar.p*Oscar.q*(Oscar.Jx - Oscar.Jy) + Oscar.U4)/Oscar.Jz;

%% Calculating p,q,r

Oscar.p = Oscar.p_dot*Oscar.Ts+Oscar.p;
Oscar.q = Oscar.q_dot*Oscar.Ts+Oscar.q;
Oscar.r = Oscar.r_dot*Oscar.Ts+Oscar.r;

%% MAJ angular velocities in the world frame

Oscar.phi_dot   = Oscar.p + sin(Oscar.phi)*tan(Oscar.theta)*Oscar.q + cos(Oscar.phi)*tan(Oscar.theta)*Oscar.r;
Oscar.theta_dot = cos(Oscar.phi)*Oscar.q - sin(Oscar.phi)*Oscar.r;
Oscar.psi_dot   = sin(Oscar.phi)/cos(Oscar.theta)*Oscar.q + cos(Oscar.phi)/cos(Oscar.theta)*Oscar.r;

%% MAJ Disturbance model (if you want disturbance, please add on InitVariables)

% ground effect + lift + drag + ...
Oscar.X_ddot = Oscar.X_ddot + Oscar.X_dis/Oscar.m; 
Oscar.Y_ddot = Oscar.Y_ddot + Oscar.Y_dis/Oscar.m; 
Oscar.Z_ddot = Oscar.Z_ddot + Oscar.Z_dis/Oscar.m; 
Oscar.phi_dot = Oscar.phi_dot + Oscar.phi_dis/Oscar.Jx*Oscar.Ts; 
Oscar.theta_dot = Oscar.theta_dot + Oscar.theta_dis/Oscar.Jy*Oscar.Ts; 
Oscar.psi_dot = Oscar.psi_dot + Oscar.psi_dis/Oscar.Jz*Oscar.Ts;

%% MAJ Update Velocities and Positions (Discrete Integration)

% Calculating the Z velocity & position
Oscar.Z_dot = Oscar.Z_ddot*Oscar.Ts + Oscar.Z_dot;
Oscar.Z = Oscar.Z_dot*Oscar.Ts + Oscar.Z;

% Calculating the X velocity & position
Oscar.X_dot = Oscar.X_ddot*Oscar.Ts + Oscar.X_dot;
Oscar.X = Oscar.X_dot*Oscar.Ts + Oscar.X;

% Calculating the Y velocity & position
Oscar.Y_dot = Oscar.Y_ddot*Oscar.Ts + Oscar.Y_dot;
Oscar.Y = Oscar.Y_dot*Oscar.Ts + Oscar.Y;

% Calculating angular position
Oscar.phi = Oscar.phi_dot*Oscar.Ts + Oscar.phi;
Oscar.theta = Oscar.theta_dot*Oscar.Ts+Oscar.theta;
Oscar.psi = Oscar.psi_dot*Oscar.Ts+Oscar.psi;

end



