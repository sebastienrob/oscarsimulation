# Trajectory Tracking of an UAV quadrorotor 
this repository can simulate a trajectory tracking for an UAV.  
You can directly run *Main.m* to run all project.  

In *InitVariable.m", you can change all parameters :  
* *Oscar.Ts* : sampling time  
* *Oscar.sim_time* : simulation time  
* *Oscar.DurationWayPoint* : time for stabilization around Control point  
* Choice trajectory in *GenerationTrajectory*  
* *Oscar.WantNoiseMeasurment* : if you want add noises from sensors  
* others : parameters system / parameters control / parameters sensors  

After this settings, you can run *Main.m* 

## Simulation environnment
In *GenerateMatrixForDrawingQuad_OscarMat* folder, you can use your parameters  
for create matrix for drawing quadrirotor with respect of size.  

For plotting and MAJ simulation :  
* *InitPlot.m* : intialization plot figure
* *InitPlotQuadModel.m* : Initialization quadrirotor plot model  
* *MAJPlotQuadrirotor.m* : MAJ of quadrorotor simulation  
* *SaveForPlot.m* : Save variables for analytics  
* *PlotData.m* : Plotting for analytics  
* *Main.m* : comment/uncomment headind for visualize trajectory desired at each time step  

## Dynamic Model
Dynamic model (non-linear) is implement in *ModeleDynamiqueNonLineaire.m*.  
For simulation we use all Dynamic model.  
For control (PID), we use a simplified model (hyp. little angle, variation...)  
which allows to decoupling system.  

--Implement other Model as flatness property in progress--  

## Trajectory generation
Warning : you have need to choice parameters to obtain a effective trajectory.  
If point of desired trajectory is near of quadrirotor is ok.  
but the more the quadrotor will move away from the desired point,  
the more it will be difficult for him to find a good trajectory (losses in precision).  
So is very important to choice a good parameters trajectory (in relation to time),  
in according with the dynamic of quadrirotor.   

Trajectory generation is implement in *TrajectoryGeneration* folder.  
we can switch between different methods with change parameters in *InitVariable.m" :  
* *Oscar.PolynomeTrajectory* : polynome  
* *Oscar.LemniscapeTrajectory* : lemniscape trajectory  
* *Oscar.CircleTrajectory* : circle trajetory  

## Control
In *InitVariable.m", you can choice Control's method :  
* *Oscar.PositionPID* : Control Position with PID  
* *Oscar.PositionLMPC* : Control Position with LMPC  
* *Oscar.AltitudePID* : Control Altitude with PID  
* *Oscar.AltitudeLMPC* : Control Altitude with LMPC  
* *Oscar.AttitudePID* : Control Attitude with PID  
* *Oscar.AttitudeLMPC* : Control Attitude with LMPC  
* *Oscar.MotorsRatePID* : Control Position with PID  

### All PID control
You can find controller in *ControlAllPid* folder.  
Implementation of Position, Altitude/Attitude, Rate(need for motors)) PID.  
you can choose the parameters with different methods (Ziegler-Nichols for example)  

### MPC control (LMPC)

We use OSQP solver for LMPC control.  

You can find a MPC control in *ControlMPC* folder  
Implementation of Position, Altitude, and Attitude LMPC (use linear dynamic model).  
you need to choice horizon T, constraints and heights fot that.
you can just send a step 1 and tuning this parameters heuristically to get a good response.  

### NMPC control

We use ACADO solver for NMPC control.  

--Implement in progress--  
