% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% This function draws the quadrotor model by rotating the original Body
% Frame coordinates used to define the quadrotor in InitPlotQuadModel.m into
% Global Frame coordinates. It then moves the quadrotor to the current
% Global Position output from the quadrotor dynamics. In our NED coordinate
% frame, positive Z is negative Z in MATLAB and positive Y is negative Y in
% MATLAB. This is reflected iwth the - in the set function.

global Oscar

%% Draw the quadrotor arms and rotors
[Oscar.Xtemp,Oscar.Ytemp,Oscar.Ztemp]=RotateBFtoWF(Oscar.X_armX,Oscar.X_armY,Oscar.X_armZ,Oscar.phi,Oscar.theta,Oscar.psi);
set(Oscar.X_arm,'xdata',Oscar.Xtemp+Oscar.X,'ydata',-(Oscar.Ytemp+Oscar.Y),'zdata',-(Oscar.Ztemp+Oscar.Z))

[Oscar.Xtemp,Oscar.Ytemp,Oscar.Ztemp]=RotateBFtoWF(Oscar.Y_armX,Oscar.Y_armY,Oscar.Y_armZ,Oscar.phi,Oscar.theta,Oscar.psi);
set(Oscar.Y_arm,'xdata',Oscar.Xtemp+Oscar.X,'ydata',-(Oscar.Ytemp+Oscar.Y),'zdata',-(Oscar.Ztemp+Oscar.Z))

[Oscar.Xtemp,Oscar.Ytemp,Oscar.Ztemp]=RotateBFtoWF(Oscar.Motor1X,Oscar.Motor1Y,Oscar.Motor1Z,Oscar.phi,Oscar.theta,Oscar.psi);
set(Oscar.Motor1,'xdata',Oscar.Xtemp+Oscar.X,'ydata',-(Oscar.Ytemp+Oscar.Y),'zdata',-(Oscar.Ztemp+Oscar.Z-2*Oscar.t))

[Oscar.Xtemp,Oscar.Ytemp,Oscar.Ztemp]=RotateBFtoWF(Oscar.Motor2X,Oscar.Motor2Y,Oscar.Motor2Z,Oscar.phi,Oscar.theta,Oscar.psi);
set(Oscar.Motor2,'xdata',Oscar.Xtemp+Oscar.X,'ydata',-(Oscar.Ytemp+Oscar.Y),'zdata',-(Oscar.Ztemp+Oscar.Z-2*Oscar.t))

[Oscar.Xtemp,Oscar.Ytemp,Oscar.Ztemp]=RotateBFtoWF(Oscar.Motor3X,Oscar.Motor3Y,Oscar.Motor3Z,Oscar.phi,Oscar.theta,Oscar.psi);
set(Oscar.Motor3,'xdata',Oscar.Xtemp+Oscar.X,'ydata',-(Oscar.Ytemp+Oscar.Y),'zdata',-(Oscar.Ztemp+Oscar.Z-2*Oscar.t))

[Oscar.Xtemp,Oscar.Ytemp,Oscar.Ztemp]=RotateBFtoWF(Oscar.Motor4X,Oscar.Motor4Y,Oscar.Motor4Z,Oscar.phi,Oscar.theta,Oscar.psi);
set(Oscar.Motor4,'xdata',Oscar.Xtemp+Oscar.X,'ydata',-(Oscar.Ytemp+Oscar.Y),'zdata',-(Oscar.Ztemp+Oscar.Z-2*Oscar.t))