% Sébastien Petit (root on wil selby's work)
% Montpellier, France
% email : sebastien.petit.rob@gmail.com

% This script outputs can plot variables for analysis
% you can display or not with loop if

%% Plot Translational Positions

% X,Y,Z et Xd, Yd, Zd
if(1)
figure;
subplot(1,3,1);
plot(Oscar.t_plot,Oscar.X_plot)
hold on;
plot(Oscar.t_plot,Oscar.X_des_plot) 
title('X/Xd Position');
xlabel('Time (s)');
ylabel('Distance (m)');
legend('X','Xd')
grid on;
subplot(1,3,2);
plot(Oscar.t_plot,Oscar.Y_plot)
hold on;
plot(Oscar.t_plot,Oscar.Y_des_plot) 
title('Y/Yd Position');
xlabel('Time (s)');
ylabel('Distance (m)');
legend('Y','Yd')
grid on;
subplot(1,3,3);
plot(Oscar.t_plot,Oscar.Z_plot)
hold on;
plot(Oscar.t_plot,Oscar.Z_des_plot) 
title('Z/Zd Position');
xlabel('Time (s)');
ylabel('Distance (m)');
legend('Z','Zd')
grid on;
end

% vX,vY,vZ et vXd, vYd, vZd
if(1)
figure;
subplot(1,3,1);
plot(Oscar.t_plot,Oscar.X_dot_plot)
hold on;
plot(Oscar.t_plot,Oscar.X_des_dot_plot) 
title('vX/vXd Vitesse');
xlabel('Time (s)');
ylabel('Vitesse (m/s)');
legend('vX','vXd')
grid on;
subplot(1,3,2);
plot(Oscar.t_plot,Oscar.Y_dot_plot)
hold on;
plot(Oscar.t_plot,Oscar.Y_des_dot_plot)
title('vY/vYd Vitesse');
xlabel('Time (s)');
ylabel('Vitesse (m/s)');
legend('vY','vYd')
grid on;
subplot(1,3,3);
plot(Oscar.t_plot,Oscar.Z_dot_plot)
hold on;
plot(Oscar.t_plot,Oscar.Z_des_dot_plot)
title('Z/Zd Vitesse');
xlabel('Time (s)');
ylabel('Vitesse (m/s)');
legend('vZ','vZd')
grid on;
end

% control altitude U1 (Total thrust)
if(0)
figure; 
plot(Oscar.t_plot,Oscar.U1_plot)
title('Altitude Control (U1)');
xlabel('Time (s)');
ylabel('Control Input');
grid on;
end

% accélération measurment xdotdot, ydotdot, zdotdot
if(0)
figure;
subplot(1,3,1);
plot(Oscar.t_plot,Oscar.X_ddot_meas_plot)
title('Measured Xdd');
xlabel('Time (s)');
ylabel('Distance (m)');
grid on;
subplot(1,3,2);
plot(Oscar.t_plot,Oscar.Y_ddot_meas_plot)
title('Measured Ydd');
xlabel('Time (s)');
ylabel('Distance (m)');
grid on;
subplot(1,3,3);
plot(Oscar.t_plot,Oscar.Z_ddot_meas_plot)
title('Measured Zdd');
xlabel('Time (s)');
ylabel('Control Input');
grid on;
end


%% Plot Rotational Positions

% Roll / U2
if(1)
figure;
subplot(2,1,1);
plot(Oscar.t_plot,Oscar.phi_plot)
hold on;
plot(Oscar.t_plot,Oscar.phi_ref_plot)
title('Current/Desired Roll');
xlabel('Time (s)');
ylabel('Angle (rad)');
legend('Current Roll','Desired Roll')
grid on;
subplot(2,1,2);
plot(Oscar.t_plot,Oscar.U2_plot)
title('Roll Control (U2)');
xlabel('Time (s)');
ylabel('Control Input');
grid on;
end

% Pitch / U3
if(1)
figure;
subplot(2,1,1);
plot(Oscar.t_plot,Oscar.theta_plot)
hold on;
plot(Oscar.t_plot,Oscar.theta_ref_plot)
title('Current/Desired Pitch');
xlabel('Time (s)');
ylabel('Angle (rad)');
legend('Current Pitch','Desired Pitch')
grid on;
subplot(2,1,2);
plot(Oscar.t_plot,Oscar.U3_plot)
title('Pitch Control (U3)');
xlabel('Time (s)');
ylabel('Control Input');
grid on;
end

% Yaw / U4
if(1)
figure;
subplot(2,1,1);
plot(Oscar.t_plot,Oscar.psi_plot)
hold on;
plot(Oscar.t_plot,Oscar.psi_ref_plot)
title('Current/Desired Yaw');
xlabel('Time (s)');
ylabel('Angle (rad)');
legend('Current Yaw','Desired Yaw')
grid on;
subplot(2,1,2);
plot(Oscar.t_plot,Oscar.U4_plot)
title('Yaw Control (U4)');
xlabel('Time (s)');
ylabel('Control Input');
grid on;
end

% trajetory desired and real 3D
figure;
axes('units','normalized','position',[.2 .1 .6 .8]);
axis equal
axis([-5 5 -5 5 -5 5]);
view(30,30)
grid on
hold on
camproj perspective 
camva(8)
hlight = camlight('headlight'); 
lighting gouraud
set(gcf,'Renderer','OpenGL')
line([-1 1],[0 0],[0 0])
line([0 0],[-.5 .5],[0 0],'color','r')
plot3(Oscar.X_plot,Oscar.Y_plot,Oscar.Z_plot,'r')
hold on;
plot3(Oscar.X_des_plot,Oscar.Y_des_plot,Oscar.Z_des_plot,'g')
xlabel('x')
ylabel('y')
zlabel('z')
title('simulation of quadrorotor')